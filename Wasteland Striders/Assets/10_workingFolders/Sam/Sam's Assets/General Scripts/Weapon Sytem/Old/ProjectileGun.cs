﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ProjectileGun : MonoBehaviour
{
    // bullet
    // public GameObject bullet;    

    //bullet force
    public float shootForce, upwardForce;

    //Gun Stats
    public float timeBetweeShooting, spread, reloadTime, timeBetweenShots;
    public int magazineSize, bulletsPerTap;
    public bool allowButtonHold;
    int bulletsLeft, bulletsShot;

    //bool 
    bool shooting, readyToShoot, reloading,sprinting;

    //Reference 
    public Camera fpsCam;
    public Transform attackPoint;
    public Animator anim;

    //Graphics
    public GameObject muzzleFlash;
    public TextMeshProUGUI ammunitionDisplay;

    //Recoil
    public Rigidbody playerRb;
    public float recoilForce;

   

    //ForBugs
    public bool allowInvoke = true;

        
    private void Awake()
    {        
        bulletsLeft = magazineSize;
        readyToShoot = true;        
    }
    private void Update()
    {
        MyInput();
        if (ammunitionDisplay != null) {
            ammunitionDisplay.SetText(bulletsLeft / bulletsPerTap + "/" + magazineSize / bulletsPerTap);
        }
        if(anim != null) sprinting = anim.GetBool ("isSprinting");

    }
    public void MyInput()
    {
        if (allowButtonHold) shooting = Input.GetKey(KeyCode.Mouse0);
        else shooting = Input.GetKeyDown(KeyCode.Mouse0);

        if (Input.GetKeyDown(KeyCode.R) && bulletsLeft < magazineSize &&!reloading) StartCoroutine (Reload());
        if (readyToShoot && shooting && !reloading && bulletsLeft <= 0) StartCoroutine(Reload());

        if (readyToShoot && shooting && !reloading && bulletsLeft > 0 && !sprinting)
        {
            bulletsShot = 0;
            Shoot();
        }
    }
    private void Shoot()
    {
        readyToShoot = false;

        Ray ray = fpsCam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0.5f));        
        RaycastHit hit;
        Vector3 targetPoint;
        if (Physics.Raycast(ray, out hit))
        {
            targetPoint = hit.point;
        }
        else
            targetPoint = ray.GetPoint(75); //point far away from the player

        Vector3 directionWithoutSpread = targetPoint - attackPoint.position;

        float x = Random.Range(-spread, spread);
        float y = Random.Range(-spread, spread);

        Vector3 directionWithSpread = directionWithoutSpread + new Vector3(x, y, 0);

        //Instantiate the bullet        
        var bulletToFire = AmmoPoolingSystem.Instance.GetBullet();

        //Rotation        
        bulletToFire.transform.SetParent(attackPoint);
        bulletToFire.transform.localPosition = Vector3.zero;
        bulletToFire.transform.localRotation = Quaternion.identity;        
        bulletToFire.rb.velocity = playerRb.velocity;
        bulletToFire.rb.AddForce(bulletToFire.transform.forward * shootForce, ForceMode.Impulse);
        bulletToFire.transform.SetParent(null);
        
        //Add Force
        // force bouncing bullet (upward force is not good for regular)
        // currentBullet.GetComponent<Rigidbody>().AddForce(fpsCam.transform.forward* shootForce, ForceMode.Impulse); 


        if (muzzleFlash != null)
        {
            GameObject muzzle = Instantiate(muzzleFlash, attackPoint.position, Quaternion.identity);
            Destroy(muzzle, 1f);
        }

        bulletsLeft--;
        bulletsShot++;

        if (allowInvoke)
        {
            Invoke("ResetShoot", timeBetweeShooting);
            allowInvoke = false;
            //Recoil 
            playerRb.AddForce(-directionWithSpread.normalized * recoilForce, ForceMode.Impulse);
        }
        //SHOTGUN TYPE OF SHOOTING
        if (bulletsShot < bulletsPerTap && bulletsLeft > 0)
            Invoke("Shoot", timeBetweenShots);
    }
    void ResetShoot()
    {
        readyToShoot = true;
        allowInvoke = true;
    }

    IEnumerator Reload()
    {
        reloading = true;
        anim.SetBool("isReloading", true);

        yield return new WaitForSeconds(reloadTime);

        anim.SetBool("isReloading", false);
        bulletsLeft = magazineSize;
        reloading = false;

        yield return null; 
    }

}
