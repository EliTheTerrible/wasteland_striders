﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

[CreateAssetMenu(menuName = "Run Settings")]
public class RunSettings : ScriptableObject
{
    public string shopSceneName;
    public string surfaceHubScene;
    public string[] levelScenes;
    public float numberOfLevels;
}
