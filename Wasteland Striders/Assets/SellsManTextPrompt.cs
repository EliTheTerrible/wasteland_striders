﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SellsManTextPrompt : MonoBehaviour
{
    public GameObject uiText;
    public GameObject player; 
    void Start()
    {
        uiText.SetActive(false);   
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if(player.gameObject.tag == "Player")
        {
            uiText.SetActive(true);
            StartCoroutine("DeactivateAfterAmmountofTime"); 

        }

    }
    IEnumerator DeactivateAfterAmmountofTime()
    {
        yield return new WaitForSeconds(5);
        uiText.SetActive(false); 
    }
}
