﻿using UnityEngine;
using System.Collections;

public class HitBox : MonoBehaviour
{   
    public EnemyHitDetector.BodyPart part;
    EnemyHitDetector hitDetector;

    public void Initialize(EnemyHitDetector _parentHD)
    {
        hitDetector = _parentHD;        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != "Bullet")
            return;
        hitDetector.RecieveDamgeIn(part);
    }
}
