﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGeneratorTester : MonoBehaviour
{
    public LevelGenerator generator;
    public KeyCode recreateLevelKey;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(recreateLevelKey)) {
            StartCoroutine(generator.CreateLevel());
        }
    }
}
