﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPoolingSystem : MonoBehaviour
{
    public static AmmoPoolingSystem instance;
    public List<Bullet> akBullets; 
    
    public static AmmoPoolingSystem Instance { get { return instance; } }
    public  GameObject bulletPrefab;

    public int initialPoolSize = 20;
    void Awake()
    {
        if(instance != this) {
            if(instance != null) {                
                return;
            }
            instance = this;    
        }

        akBullets = new List<Bullet>(initialPoolSize);
        for (int i = 0; i < initialPoolSize; i++)
        {
            GameObject prefabInstance = Instantiate(bulletPrefab);
            prefabInstance.transform.SetParent(transform);
            prefabInstance.SetActive(false);
            akBullets.Add(prefabInstance.GetComponent<Bullet>());
        }
    }

   public Bullet GetBullet()
    {
        foreach (Bullet bullet in akBullets)
        {
            if (!bullet.gameObject.activeInHierarchy)
            {
                bullet.gameObject.SetActive(true);                
                return bullet; 
            }
        }

        GameObject newBullet = Instantiate(bulletPrefab, transform);
        var newBulletImpact = newBullet.GetComponent<Bullet>();
        akBullets.Add(newBulletImpact);
        return newBulletImpact; 
    }
}
