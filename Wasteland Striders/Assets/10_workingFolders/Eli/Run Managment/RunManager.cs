﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunManager : MonoBehaviour
{
    public static RunManager current;
    public enum GameState { surface, run}
    public GameState state;
    public RunSettings settings;    
    int levelCounter = 0;    
    private void OnEnable()
    {        
        if(current != this) {
            if (current != null)
            {
                return;
            }
            current = this;
        }        
    }

    [ContextMenu("StartRun")]
    public void StartRun()
    {
        if (settings == null)
        {
            if(settings.levelScenes == null || settings.levelScenes.Length == 0)
            {
                Debug.Log("No level scenes added!");
                return;
            }
            Debug.Log("No run settings set!");
            return;
        }
        state = GameState.run;
        levelCounter = 1;
        var playerGameObject = new List<GameObject>();
        playerGameObject.Add(transform.root.gameObject);
        SceneTransitionController.current.StartTransion(settings.levelScenes[0], playerGameObject);
        LevelManager.current = null;
    }
    
    public void advanceToNextLevel()
    {
        var playerGameObject = new List<GameObject>();
        playerGameObject.Add(transform.root.gameObject);
        if (levelCounter == settings.numberOfLevels) {
            // return to hub once completed required number of levels
            state = GameState.surface;
            levelCounter = 0;            
            SceneTransitionController.current.StartTransion(settings.surfaceHubScene, playerGameObject);
        }
        else {
            // regenerate level and add 1 to  level counter
            levelCounter++;
            SceneTransitionController.current.StartTransion(settings.shopSceneName, playerGameObject);
        }
        
    }
    public void resetLevel()
    {
        var playerGameObject = new List<GameObject>();
        playerGameObject.Add(transform.root.gameObject);       
        SceneTransitionController.current.StartTransion(settings.levelScenes[0], playerGameObject);
    }
}
