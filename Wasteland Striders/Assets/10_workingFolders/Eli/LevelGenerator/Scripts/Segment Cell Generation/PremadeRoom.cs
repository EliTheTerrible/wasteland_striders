﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct PointOfEntry
{
    public Vector2Int address;
    public MovementDirections wall;    
}
public struct RoomPathConnectionInfo
{    
    public bool connected;
    public Dictionary<MovementDirections, List<PointOfEntryInfo>> poeInfo;    
}
public struct PointOfEntryInfo
{
    public PointOfEntry poe;
    public MovementDirections possibleRoomRotaion;
    public List<PathStepInfo> createdPath;
}
public struct PathStepInfo
{
    public Vector2Int coords;
    public List<MovementDirections> availableDirs;
}
public class PremadeRoom : SegmentCell
{
    public Vector2Int roomSize;
    public PointOfEntry[] pointsOfEntry;
    public Transform[] enemySpawns;
}
