﻿using UnityEngine;
using System.Collections;
using TMPro;

public class PlayerMover : MonoBehaviour
{
    public static PlayerMover current;
    public KeyCode springKey;
    private Rigidbody rb;    
    public CharacterController controller;    
    public Transform fpsCam, buttomMarker;    
    Transform _transform;
    public int lookSensitivity = 100;
    public bool invert;
    public float runSpeed, sprintSpeed, margin;
    public TMP_Text vendorText;
    public TMP_Text enlistText;
    public TMP_Text startText;
    [Header("Jump Settings")]
    public AnimationCurve jumpCurve;
    public ForceMode jumpForceType;
    public float jumpTime;
    public int jumoforce;
    public bool OnGround;
    bool jumping;
    float currentCameraRoation = 0;
    private void OnEnable()
    {
        _transform = transform;
        if(LevelManager.player == null)
        {
            LevelManager.player = this;
            current = this;
        }        
        else if(LevelManager.player != this && LevelManager.player != null)
        {
            Debug.Log("Destroying PLayer");
            Destroy(gameObject);
            return;
        }

        Cursor.lockState = CursorLockMode.Locked;                        
        if(TryGetComponent<Rigidbody>(out rb)) {
            if (rb == null)
                return;
        }        
        StartCoroutine(GroundCheck());                
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void Update()
    {          
        var notPaused = (PauseController.current != null && !PauseController.current.pause) || (PauseController.current == null);                
        Look();            
        if (notPaused)
        {
            Move();
            if (Input.GetKeyDown(KeyCode.Space))
            {                
                if (OnGround)
                {
                    StartCoroutine(Jump(false));
                }
                else if(!jumping)
                {
                    Ray height = new Ray(buttomMarker.position, -buttomMarker.up);
                    RaycastHit ground;
                    if (Physics.Raycast(height, out ground))
                    {
                        if (ground.distance <= margin)
                        {                            
                            StartCoroutine(Jump(true));
                        }
                    }
                }
            }
        }        
    }

    private void Look()
    {
        var aim_x = Input.GetAxis("Mouse X") * lookSensitivity * Time.deltaTime;
        var aim_y = -Input.GetAxis("Mouse Y") * lookSensitivity * Time.deltaTime;

        currentCameraRoation = invert? currentCameraRoation - aim_y : currentCameraRoation + aim_y;
        currentCameraRoation = Mathf.Clamp(currentCameraRoation, -90f, 90f);
        _transform.transform.Rotate(0f, aim_x, 0f);
        fpsCam.transform.localRotation = Quaternion.Euler(currentCameraRoation, 0f, 0f);
        
    }

    void Move()
    {
        var input_walk = Input.GetAxis("Vertical");
        var input_strafe = Input.GetAxis("Horizontal");
        var movementDirection = (_transform.forward * input_walk + _transform.transform.right * input_strafe);
        var speed = !Input.GetKey(springKey) ? runSpeed : sprintSpeed;
        if(OnGround)
        {
            controller.Move(movementDirection * speed * Time.deltaTime);
        }
        else
        {
            rb.AddForce(movementDirection * speed * Time.deltaTime);
        }
    }

    IEnumerator Jump(bool midAir)
    {
        jumping = true;
        float timer = 0f;
        rb.isKinematic = false;
        controller.enabled = false;        
        _transform.Translate(0f, margin, 0f);         
        if(!midAir)
            rb.velocity = controller.velocity + new Vector3(0f, jumoforce - controller.velocity.y, 0f);
        while(timer <=jumpTime)
        {
            var jumpCoe = jumpCurve.Evaluate(timer / jumpTime);
            if (jumpCoe <= 0f)
                break;
            rb.AddForce(_transform.up * jumoforce *jumpCoe * Time.deltaTime, jumpForceType);
            timer += Time.deltaTime;
            yield return null;
        }
        timer = 0f;
        while(!OnGround && timer <= jumpTime)
        {
            rb.AddForce(-_transform.up * (jumoforce*2) * Time.deltaTime, jumpForceType);
            timer += Time.deltaTime;
            yield return null;
        }
        jumping = false;
    }    
    public void SetPuase(bool _isPaused)
    {        
        controller.enabled = _isPaused ? false : true;
    }
    public Vector2 Get2DVelocity()
    {
        if (OnGround)        
            return new Vector2(_transform.InverseTransformDirection(controller.velocity).x, _transform.InverseTransformDirection(controller.velocity).z) / sprintSpeed;        
        else
            return new Vector2(_transform.InverseTransformDirection(rb.velocity).x, _transform.InverseTransformDirection(rb.velocity).z) / sprintSpeed;
    }
    public IEnumerator GroundCheck()
    {
        while(enabled) {
            Ray height = new Ray(buttomMarker.position, -buttomMarker.up);
            RaycastHit ground;            
            if(Physics.Raycast(height, out ground)) {                
                if (ground.distance > margin) {                    
                    rb.isKinematic = false;
                    controller.enabled = false;
                    if (OnGround)
                        rb.velocity = controller.velocity;
                    OnGround = false;
                }
                else
                {
                    rb.isKinematic = true;
                    controller.enabled = true;                        
                    OnGround = true;
                }
            }                            
            yield return null;
        }
    }
}
