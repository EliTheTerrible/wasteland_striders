﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class LevelManager : MonoBehaviour
{
    public enum Area { Surface, Shop, Level }
    public Area area;

    public static LevelManager current;    
    public static PlayerMover player;
    public PlayerMover _player;
    public bool autoStart = false;
    public Dictionary<GameObject, EnemyController> activeEnemies = new Dictionary<GameObject, EnemyController>();
    public bool inExitZone = false;
    public virtual void OnEnable()
    {        
        if(current != this) {
            if(current != null) {
                Destroy(current.gameObject);
            }
            current = this;
        }        

        if(area == Area.Level)
        {
            if(player != null) { 
                    player.gameObject.SetActive(false);
                    RunManager.current.state = RunManager.GameState.run;
            }        
            if(autoStart) {
                StartCoroutine(GeneraterLevelWhenPossible());
            }
        }        
    }

    IEnumerator GeneraterLevelWhenPossible()
    {                
        while (LevelGenerator.current == null) {
            yield return null;
        }       
        StartCoroutine(LevelGenerator.current.CreateLevel());
        yield return null;
    }

    public virtual void Update()
    {
        if(RunManager.current != null) {
            if (inExitZone && Input.GetKeyDown(KeyCode.Return))
                RunManager.current.advanceToNextLevel();
        }
    }
}
