﻿using UnityEngine;

public class CellDoor : CellEdge {

	private static Quaternion
		normalRotation = Quaternion.Euler(0f, -90f, 0f),
		mirroredRotation = Quaternion.Euler(0f, 90f, 0f);

	public Transform hinge;

	private bool isMirrored;

	private CellDoor OtherSideOfDoor {
		get {
			return otherCell.GetEdge(direction.GetOpposite()) as CellDoor;
		}
	}
	
	public override void Initialize (SegmentCell primary, SegmentCell other, MovementDirections direction) 
    {
		base.Initialize(primary, other, direction);           
	}    
}