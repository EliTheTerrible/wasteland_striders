﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(WeaponIdentifier))]
public class WeaponBehaviour : MonoBehaviour
{
    public AudioSource source_Fire;
    public AudioSource source_reload;
    public AudioClip[] sound_shots;
    public AudioClip sound_reload;
    public Transform firePoint;
    public float muzzleVelocity;

    public float reloadTime;
    public int magazineCap;
    public int inMag;

    

    public float rof_rpm = 60;
    public bool auto;
    public bool melee;
    
    bool projectileSpawned = false;
    [Range(0f,1f)]public float spreadFactor = 0f;

    WeaponIdentifier identifier;    
    public void OnEnable()
    {
        if (identifier == null)
            identifier = GetComponent<WeaponIdentifier>();                
        inMag = magazineCap;
    }
    public void OnDisable()
    {
        PlayerAnimationController.current.ResetToIdle();             
    }

    public void StartReload()
    {
        if (PlayerAnimationController.current.IsReloading())
            return;
        source_reload.clip = sound_reload;
        source_reload.Play();
        StartCoroutine(Reload());
    }
    IEnumerator Reload()
    {        
        StopFireAnimation();
        inMag = magazineCap;
        PlayerAnimationController.current.TriggerReload(reloadTime);
        yield return new WaitUntil(() => !PlayerAnimationController.current.IsReloading());
        PlayerAnimationController.current.ResetToIdle();
    }


    #region Fire Control
    public void Fire(bool _down)
    {        
        if(inMag > 0)
        {
            if(auto)
            {
                StartCoroutine(FireSequence());        
            }
            else if(_down)
            {
                StartCoroutine(FireSequence());
            }
        }
        else
        {
            
            StartReload();                        
        }
    }
    IEnumerator FireSequence()
    {
        projectileSpawned = false;
        yield return TriggerFireAnimation();
        if(!auto || !Input.GetButton("Fire1"))
            StopFireAnimation();                        
    }

    private void StopFireAnimation()
    {       
        PlayerAnimationController.current.SetAutoFire(false);
        PlayerAnimationController.current.ExitFire();        
    }
    private IEnumerator TriggerFireAnimation()
    {                
        var _spread = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));        
        PlayerAnimationController.current.SetSpread(_spread, spreadFactor);
        PlayerAnimationController.current.TriggerFire();
        PlayerAnimationController.current.SetAutoFire(auto);
        PlayerAnimationController.current.SetRateOfFire(rof_rpm);        
        WaitUntil EndOfFire = new WaitUntil(() => !PlayerAnimationController.current.IsFiring());
        yield return EndOfFire;
    }
    #endregion

    public void SpawnProjectile()
    {
        source_Fire.clip = sound_shots[Random.Range(0, sound_shots.Length)];
        source_Fire.Play();
        if(firePoint != null && !projectileSpawned)
        {
            var newBullet = AmmoPoolingSystem.Instance.GetBullet();
            newBullet.transform.position = firePoint.position;
            newBullet.transform.rotation = firePoint.rotation;
            newBullet.rb.velocity = newBullet.transform.forward * muzzleVelocity;
            newBullet.gameObject.SetActive(true);
            --inMag;
            projectileSpawned = true;
        }
    }
}
