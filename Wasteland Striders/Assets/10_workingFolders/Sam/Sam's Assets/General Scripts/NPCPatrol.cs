﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class NPCPatrol : MonoBehaviour
{
    public float rotateSpeed = 2.0f;

    private bool rotating;
    public Transform[] patrol;
    private int Currentpoint;
    public float moveSpeed;

    void Start()
    {
        transform.position = patrol[0].position;
        Currentpoint = 0;
    }
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, patrol[Currentpoint].position, moveSpeed * Time.deltaTime);
        if (Vector3.Distance(transform.position, patrol[Currentpoint].position) < 0.5f)
        {
            StartCoroutine(TurnTowards(transform.forward));
            Currentpoint++;
        }

        if (Currentpoint == patrol.Length)
        {
            Currentpoint = 0;
        }
    }
   
    IEnumerator TurnTowards(Vector3 lookAtTarget)
    {

        if (rotating == false)
        {
           
            Quaternion newRotation = Quaternion.LookRotation(lookAtTarget - transform.position);
            newRotation.x = 0;
            newRotation.z = 0;
            

            for (float u = 0.0f; u <= 1.0f; u += Time.deltaTime * rotateSpeed)
            {
                rotating = true;
                transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, u);

                yield return null;
            }
            rotating = false;
        }
    }
}

