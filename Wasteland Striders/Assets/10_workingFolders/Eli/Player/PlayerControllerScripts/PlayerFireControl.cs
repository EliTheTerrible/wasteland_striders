﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFireControl : MonoBehaviour
{    
    public bool debug_holdFire;
    private void Update()
    {
        var currentWeapon = WeaponSwitcher.current.GetCurrentWeapon();
        if (currentWeapon != null)
        {            
            var fire = Input.GetButton("Fire1") && PlayerAnimationController.current.ReadyToFire();
            var canReload = currentWeapon.behaviour.inMag < currentWeapon.behaviour.magazineCap && !PlayerAnimationController.current.IsReloading() ;

            if (fire || debug_holdFire)
            {
                if(WeaponSwitcher.current.GetCurrentWeapon() != null)
                {
                    var buttonDownFrame = Input.GetButtonDown("Fire1");
                    WeaponSwitcher.current.GetCurrentWeapon().behaviour.Fire(buttonDownFrame || debug_holdFire);
                }
            }

            if (Input.GetKeyDown(KeyCode.R) && canReload)
            {
                currentWeapon.behaviour.StartReload();
            }
        }
        else
        {
            Debug.Log("No Weapon");
        }
    }
}
