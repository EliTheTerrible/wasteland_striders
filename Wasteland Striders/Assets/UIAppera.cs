﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIAppera : MonoBehaviour
{
    public bool vendor, start, enlisted;


    [SerializeField] private TMP_Text coustomtext;

    public void OnEnable()
    {
        if (vendor)
            coustomtext = LevelManager.player.vendorText;
        else if (start)
            coustomtext = LevelManager.player.startText;
        else if (enlisted)
            coustomtext = LevelManager.player.startText;
    }

    private void OnDisable()
    {
        coustomtext.enabled = false;        
    }
        
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            coustomtext.enabled = true;

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            coustomtext.enabled = false;


        }
    }    
}