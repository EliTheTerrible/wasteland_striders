﻿using UnityEngine;
using System.Collections;

public class EnemyHealthController : MonoBehaviour
{
    [Header("Exposed Properties")]
    [SerializeField] EnemyController controller;    
    [SerializeField] float health;
    [SerializeField] float limbMultiplier, headMultiplier;

    public void Initialize(EnemySettings _settings) {
        if (controller == null)
            controller = GetComponent<EnemyController>();        
        if(_settings == null)
        {
            Debug.LogWarning("No Enenmy Settings Found");
            return;
        }
        health = _settings.startHealth;
        limbMultiplier = _settings.limbsMultiplier;
        headMultiplier = _settings.headMultiplier;
    }

    public void TakeTorsoDamage(float damage) {
        health -= damage;
        if(health <= 0) {
            Die();
        }
    }
    public void TakeLimbDamage(float damage)
    {
        health -= damage * limbMultiplier;        
        if (health <= 0) {
            Die();
        }
    }
    public void TakeHeadDamage(float damage)
    {
        health -= damage * headMultiplier;
        if (health <= 0) {
            Die();
        }
    }
    [ContextMenu("Die")]
    void Die() {
        controller.enabled = false;
        controller.animator.enabled = false;
        controller.DisableControllers();
    }
}
