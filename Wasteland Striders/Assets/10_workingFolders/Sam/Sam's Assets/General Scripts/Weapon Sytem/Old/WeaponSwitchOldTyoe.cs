﻿/*
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponSwitchOldTyoe : MonoBehaviour
{
    Dictionary<string, RectTransform> availableIcons = new Dictionary<string, RectTransform>();
    Dictionary<int, List<Weapon>> availableWeapons = new Dictionary<int, List<Weapon>>();

    public static WeaponSwitchOldTyoe current;
    public Transform[] weaponHolders;
    public Transform weaponStorage;
    public RectTransform selectedWeaponIconHolder, nextWeaponPreviewIconHolder, previousWeaponPreviewIconHolder;
    public bool mouseScroll = false;
    public float scrollPreviewDuration = 2f;
    public float scrollActivationDuration = 0.3f;
    public float startOffsetX = 200f;
    public float startOffsetY = 300f;
    public TMPro.TMP_Text debugText;
    float timer = 0f;
    RectTransform selectedWeaponIcon, nextWeaponPreviewIcon, previousWeaponPreviewIcon;
    Weapon currentWeapon = null;
    [SerializeField] bool isScrolling;
    [SerializeField] bool showInfo;
    void Start()
    {
        if (current != this)
        {
            if (current != null)
            {
                Destroy(current.gameObject);
            }
            current = this;
        }

        if (weaponStorage.childCount != 0)
        {
            IndexExistingWeapons();
            SortAvailableWeapons();


            var _type = Attack_Animation_System.E_Weapon_Selection.Melee;
            var _typeName = (int)_type;
            Attack_Animation_System.current.WeaponSwitchEnum(_type);

            if (availableWeapons.ContainsKey(_typeName))
            {
                SwitchToFirst(availableWeapons[_typeName]);
                UpdateSelectedIcon();
            }
        }
        if (nextWeaponPreviewIconHolder != null)
            nextWeaponPreviewIconHolder.parent.gameObject.SetActive(false);
        if (previousWeaponPreviewIconHolder != null)
            previousWeaponPreviewIconHolder.parent.gameObject.SetActive(false);
    }

    private void SortAvailableWeapons()
    {
        foreach (int _weaponClass in availableWeapons.Keys)
        {
            foreach (Weapon _weaponToSort in availableWeapons[_weaponClass])
            {

                if (_weaponClass == (int)Attack_Animation_System.E_Weapon_Selection.Melee)
                {
                    int _weaponClassPositionIndex = 0;
                    _weaponToSort.transform.parent = weaponHolders[_weaponClassPositionIndex];
                }
                else if (_weaponClass == (int)Attack_Animation_System.E_Weapon_Selection.HandGun)
                {
                    int _weaponClassPositionIndex = 1;
                    _weaponToSort.transform.parent = weaponHolders[_weaponClassPositionIndex];
                }
                else if (_weaponClass == (int)Attack_Animation_System.E_Weapon_Selection.AssaulRifle)
                {
                    int _weaponClassPositionIndex = 2;
                    _weaponToSort.transform.parent = weaponHolders[_weaponClassPositionIndex];
                }
                else if (_weaponClass == (int)Attack_Animation_System.E_Weapon_Selection.SawedOff)
                {
                    int _weaponClassPositionIndex = 3;
                    _weaponToSort.transform.parent = weaponHolders[_weaponClassPositionIndex];
                }
            }
        }
    }

    private void IndexExistingWeapons()
    {
        for (int i = 0; i < weaponStorage.childCount; i++)
        {
            var _weapon = weaponStorage.GetChild(i).GetComponent<Weapon>();

            if (_weapon == null)
                continue;

            var _weaponType = (int)_weapon.type;
            if (availableWeapons.ContainsKey(_weaponType))
            {
                availableWeapons[_weaponType].Add(_weapon);
            }
            else
            {
                var newWeaponList = new List<Weapon>();
                availableWeapons.Add(_weaponType, newWeaponList);
                availableWeapons[_weaponType].Add(_weapon);
            }


            _weapon.gameObject.SetActive(false);

        }
    }


    void Update()
    {
        if (weaponStorage != null)
        {
            if (mouseScroll)
                CheckMousescrollSwitch();

            CheckAlphanumaricSwitch();
        }
        if (currentWeapon != null && showInfo)
        {
            debugText.text = ("Current Weapon: " + currentWeapon.weaponName
                              + "\nMouse Whell Delta : " + Input.GetAxisRaw("Mouse ScrollWheel")
                              + "\nCurrent Weapon Class: " + currentWeapon.type.ToString())
                              + "\nNumber of weapons in Class: " + availableWeapons[(int)currentWeapon.type].Count
                              + "\nCurrent Weapon Position in Class: " + GetPositionInClass();
        }
    }

    private void CheckMousescrollSwitch()
    {
        var _mouseScrollInput = Input.GetAxisRaw("Mouse ScrollWheel");
        var _currentWeaponTypeIndex = (int)currentWeapon.type;
        var _currentWeaponIndexInClass = GetPositionInClass();
        if (_mouseScrollInput != 0f)
        {
            if (_mouseScrollInput > 0)
            {
                // Select Next Weapon
                if (_currentWeaponIndexInClass == availableWeapons[_currentWeaponTypeIndex].Count - 1)
                {
                    // Swithc To Next Type
                    var _nextTypeIndex = (_currentWeaponTypeIndex + 1) % availableWeapons.Count;
                    var _nextType = (Attack_Animation_System.E_Weapon_Selection)_nextTypeIndex;
                    SelectNextOfType(_nextType);
                }
                else
                {
                    SelectNextOfType(currentWeapon.type);
                }
                UpdateScrollPreviewIcons();
                UpdateSelectedIcon();
            }
            else if (_mouseScrollInput < 0)
            {
                // Select Previous Weapon
                if (_currentWeaponIndexInClass == 0)
                {
                    var _previousTypIndex = ((int)currentWeapon.type - 1);
                    if (_previousTypIndex < 0) _previousTypIndex = availableWeapons.Count - 1;

                    var _previousType = (Attack_Animation_System.E_Weapon_Selection)_previousTypIndex;
                    SelectPreviousOfType(_previousType);
                }
                else
                {
                    SelectPreviousOfType(currentWeapon.type);
                }
                UpdateScrollPreviewIcons();
                UpdateSelectedIcon();
            }
            if (!isScrolling)
            {
                StartCoroutine(ShowScrollPreview());
                isScrolling = true;
            }
            timer = 0f;
        }
        else if (isScrolling)
        {
            timer += Time.deltaTime;
            if (timer >= scrollPreviewDuration)
            {
                isScrolling = false;
            }
        }
    }
    private int GetPositionInClass()
    {
        for (int i = 0; i < availableWeapons[(int)currentWeapon.type].Count; ++i)
            if (availableWeapons[(int)currentWeapon.type][i] == currentWeapon)
                return i;
        return -1;
    }

    private void CheckAlphanumaricSwitch()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Attack_Animation_System.E_Weapon_Selection typeToSelect = Attack_Animation_System.E_Weapon_Selection.Melee;
            SelectNextOfType(typeToSelect);

        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Attack_Animation_System.E_Weapon_Selection typeToSelect = Attack_Animation_System.E_Weapon_Selection.HandGun;
            SelectNextOfType(typeToSelect);

        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            Attack_Animation_System.E_Weapon_Selection typeToSelect = Attack_Animation_System.E_Weapon_Selection.AssaulRifle;
            SelectNextOfType(typeToSelect);

        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            Attack_Animation_System.E_Weapon_Selection typeToSelect = Attack_Animation_System.E_Weapon_Selection.SawedOff;
            SelectNextOfType(typeToSelect);
        }
    }


    IEnumerator ShowScrollPreview()
    {
        nextWeaponPreviewIconHolder.parent.gameObject.SetActive(true);
        previousWeaponPreviewIconHolder.parent.gameObject.SetActive(true);

        var startPositionNext = selectedWeaponIconHolder.parent.position + new Vector3(startOffsetX, 150f, 0f);
        var startPositionPrevious = selectedWeaponIconHolder.parent.position + new Vector3(-300f, -startOffsetY, 0f);
        var endPositionRefrence = selectedWeaponIconHolder.parent.position;

        float _timeElapse = 0f;


        nextWeaponPreviewIconHolder.parent.position = startPositionNext;
        previousWeaponPreviewIconHolder.parent.position = startPositionPrevious;

        while (_timeElapse <= scrollActivationDuration)
        {
            _timeElapse += Time.deltaTime;
            var _nextPrevieUpdatedPosition = Mathf.Lerp(startPositionNext.x, endPositionRefrence.x, _timeElapse / scrollActivationDuration);
            var _previousPrevieUpdatedPosition = Mathf.Lerp(startPositionPrevious.y, endPositionRefrence.y, _timeElapse / scrollActivationDuration);
            nextWeaponPreviewIconHolder.parent.position = new Vector3(_nextPrevieUpdatedPosition, selectedWeaponIconHolder.position.y + 150f, 0f);
            previousWeaponPreviewIconHolder.parent.position = new Vector3(selectedWeaponIconHolder.position.x - 300f, _previousPrevieUpdatedPosition, 0f);
            yield return null;
        }
        while (isScrolling)
        {

            yield return null;
        }
        _timeElapse = 0f;
        while (!isScrolling && _timeElapse <= scrollActivationDuration)
        {
            if (isScrolling)
                _timeElapse = 0f;
            _timeElapse += Time.deltaTime;
            var t = _timeElapse / scrollActivationDuration;
            var _nextPrevieUpdatedPosition = Mathf.Lerp(endPositionRefrence.x, startPositionNext.x, t);
            var _previousPrevieUpdatedPosition = Mathf.Lerp(endPositionRefrence.y, startPositionPrevious.y, t);

            nextWeaponPreviewIconHolder.parent.position = new Vector3(_nextPrevieUpdatedPosition, selectedWeaponIconHolder.position.y + 150f, 0f);
            previousWeaponPreviewIconHolder.parent.position = new Vector3(selectedWeaponIconHolder.position.x - 300f, _previousPrevieUpdatedPosition, 0f);
            yield return new WaitForEndOfFrame();
        }
        yield return null;
    }

    #region weapon selection and switching mehthods
    private void SelectNextOfType(Attack_Animation_System.E_Weapon_Selection Type)
    {
        var _typeName = (int)Type;

        if (availableWeapons.ContainsKey(_typeName))
        {
            var weaponsOfType = availableWeapons[_typeName];
            if (currentWeapon.type != Type)
            {
                SwitchToFirst(weaponsOfType);
            }
            else
            {
                SwitchToNextOfType(weaponsOfType);
            }
            UpdateSelectedIcon();
            Attack_Animation_System.current.WeaponSwitchEnum(Type);
        }
    }
    private void SelectPreviousOfType(Attack_Animation_System.E_Weapon_Selection Type)
    {
        var _typeName = (int)Type;

        if (availableWeapons.ContainsKey(_typeName))
        {
            List<Weapon> weaponsOfType = availableWeapons[_typeName];
            if (currentWeapon.type != Type)
            {
                SwitchToLast(weaponsOfType);
            }
            else
            {
                SwitchToPreviousOfType(weaponsOfType);
            }
            UpdateSelectedIcon();
            Attack_Animation_System.current.WeaponSwitchEnum(Type);
        }
    }
    private void SwitchToNextOfType(List<Weapon> weaponsOfType)
    {
        if (currentWeapon != null)
        {
            currentWeapon.gameObject.name = "Unselected_" + currentWeapon.weaponName;
            currentWeapon.gameObject.SetActive(false);
        }
        for (int i = 0; i < weaponsOfType.Count; ++i)
        {
            if (weaponsOfType[i] == currentWeapon)
            {
                if (i == weaponsOfType.Count - 1)
                {
                    currentWeapon = weaponsOfType[0];

                }
                else currentWeapon = weaponsOfType[i + 1];
                currentWeapon.gameObject.name = currentWeapon.gameobjectNameInAnimation;
                currentWeapon.gameObject.SetActive(true);
                return;
            }
        }
    }
    private void SwitchToPreviousOfType(List<Weapon> weaponsOfType)
    {
        if (currentWeapon != null)
        {
            currentWeapon.gameObject.name = "Unselected_" + currentWeapon.weaponName;
            currentWeapon.gameObject.SetActive(false);
        }
        for (int i = 0; i < weaponsOfType.Count; ++i)
        {
            if (weaponsOfType[i] == currentWeapon)
            {
                if (i == 0)
                {
                    currentWeapon = weaponsOfType[weaponsOfType.Count - 1];

                }
                else currentWeapon = weaponsOfType[i - 1];
                currentWeapon.gameObject.name = currentWeapon.gameobjectNameInAnimation;
                currentWeapon.gameObject.SetActive(true);
                return;
            }
        }
    }
    public void SwitchToFirst(List<Weapon> weaponsOfType)
    {
        if (currentWeapon != null)
        {
            currentWeapon.gameObject.name = "Unselected_" + currentWeapon.weaponName;
            currentWeapon.gameObject.SetActive(false);
        }
        currentWeapon = weaponsOfType[0];
        currentWeapon.gameObject.name = currentWeapon.gameobjectNameInAnimation;
        currentWeapon.gameObject.SetActive(true);
    }
    public void SwitchToLast(List<Weapon> weaponOfType)
    {
        if (currentWeapon != null)
        {
            currentWeapon.gameObject.name = "Unselected_" + currentWeapon.weaponName;
            currentWeapon.gameObject.SetActive(false);
        }
        currentWeapon = weaponOfType[weaponOfType.Count - 1];
        currentWeapon.gameObject.name = currentWeapon.gameobjectNameInAnimation;
        currentWeapon.gameObject.SetActive(true);
    }
    #endregion

    #region icon refresh mehods
    Weapon GetNextWeapon()
    {
        var _currentWeaponTypeIndex = (int)currentWeapon.type;
        var _currentWeaponTypeCount = availableWeapons[_currentWeaponTypeIndex].Count - 1;
        var _currentWeaponIndexInList = GetPositionInClass();
        if (_currentWeaponIndexInList == _currentWeaponTypeCount)
        {
            if (_currentWeaponTypeIndex == (availableWeapons.Count - 1))
            {
                return availableWeapons[0][0];
            }
            var _nextList = availableWeapons[_currentWeaponTypeIndex + 1];
            return _nextList[0];
        }
        return availableWeapons[_currentWeaponTypeIndex][_currentWeaponIndexInList + 1];
    }
    Weapon GetPreviousWeapon()
    {
        var _currentWeaponTypeIndex = (int)currentWeapon.type;
        var _currentWeaponTypeList = availableWeapons[_currentWeaponTypeIndex];
        var _currentWeaponIndexInList = GetPositionInClass();
        if (_currentWeaponIndexInList == 0)
        {
            if (_currentWeaponTypeIndex == 0)
            {
                var _lastList = availableWeapons[availableWeapons.Count - 1];
                return _lastList[_lastList.Count - 1];
            }
            var _previousList = availableWeapons[(int)currentWeapon.type - 1];
            var _lastOfPreviouseList = _previousList[_previousList.Count - 1];
            return _lastOfPreviouseList;
        }

        return _currentWeaponTypeList[_currentWeaponIndexInList - 1];
    }

    private void UpdateScrollPreviewIcons()
    {
        if (availableIcons.ContainsKey(currentWeapon.weaponName))
        {
            ShowSelectedIcon(currentWeapon);
        }
        else
        {
            CreateNewIcon(currentWeapon);
            ShowSelectedIcon(currentWeapon);
        }

        var _nextWeapon = GetNextWeapon();
        var _previousWeapon = GetPreviousWeapon();

        if (availableIcons.ContainsKey(_nextWeapon.weaponName))
        {
            ShowNextWeaponPreview(_nextWeapon);
        }
        else
        {
            CreateNewIcon(_nextWeapon);
            ShowNextWeaponPreview(_nextWeapon);
        }

        if (availableIcons.ContainsKey(_previousWeapon.weaponName))
        {
            ShowPreviousWeaponPreview(_previousWeapon);
        }
        else
        {
            CreateNewIcon(_previousWeapon);
            ShowPreviousWeaponPreview(_previousWeapon);
        }
    }
    private void UpdateSelectedIcon()
    {
        if (availableIcons.ContainsKey(currentWeapon.weaponName))
        {
            ShowSelectedIcon(currentWeapon);
        }
        else
        {
            CreateNewIcon(currentWeapon);
            ShowSelectedIcon(currentWeapon);
        }
        selectedWeaponIcon.gameObject.SetActive(true);
    }
    private void CreateNewIcon(Weapon _iconToCreate)
    {
        GameObject newIcon = Instantiate(_iconToCreate.iconPrefab);
        newIcon.name = _iconToCreate.weaponName;
        newIcon.transform.SetParent(selectedWeaponIconHolder);
        newIcon.transform.localPosition = Vector3.zero;
        availableIcons.Add(_iconToCreate.weaponName, (RectTransform)newIcon.transform);
    }

    private void ShowSelectedIcon(Weapon _iconToShow)
    {
        if (selectedWeaponIcon != null)
            selectedWeaponIcon.gameObject.SetActive(false);
        selectedWeaponIcon = availableIcons[_iconToShow.weaponName];
        selectedWeaponIcon.SetParent((Transform)selectedWeaponIconHolder);
        selectedWeaponIcon.localPosition = Vector3.zero;
        selectedWeaponIcon.gameObject.SetActive(true);
    }

    private void ShowNextWeaponPreview(Weapon _iconToShow)
    {
        if (nextWeaponPreviewIcon != null && nextWeaponPreviewIcon != currentWeapon)
            nextWeaponPreviewIcon.gameObject.SetActive(false);
        nextWeaponPreviewIcon = availableIcons[_iconToShow.weaponName];
        nextWeaponPreviewIcon.SetParent((Transform)nextWeaponPreviewIconHolder);
        nextWeaponPreviewIcon.localPosition = Vector3.zero;
        nextWeaponPreviewIcon.gameObject.SetActive(true);
    }

    private void ShowPreviousWeaponPreview(Weapon _iconToShow)
    {
        if (previousWeaponPreviewIcon != null && previousWeaponPreviewIcon != currentWeapon)
            previousWeaponPreviewIcon.gameObject.SetActive(false);
        previousWeaponPreviewIcon = availableIcons[_iconToShow.weaponName];
        previousWeaponPreviewIcon.SetParent((Transform)previousWeaponPreviewIconHolder);
        previousWeaponPreviewIcon.localPosition = Vector3.zero;
        previousWeaponPreviewIcon.gameObject.SetActive(true);
    }
    #endregion
}
*/