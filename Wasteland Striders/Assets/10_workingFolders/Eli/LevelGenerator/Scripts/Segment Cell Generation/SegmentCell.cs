﻿using UnityEngine;
using UnityEngine.AI;

public class SegmentCell : MonoBehaviour {

	public Vector2Int coordinates;
    public LevelSegmentGenerator generator;
    public bool atEdge = false;   
    public SegmentRoom room;
    public NavMeshLink link_f;
    public NavMeshLink link_r;
    public NavMeshLink link_b;
    public NavMeshLink link_l;

    [SerializeField]  private CellEdge[] edges = new CellEdge[MazeDirections.Count];
   
	private int initializedEdgeCount;

	public bool IsFullyInitialized {
		get {
			return initializedEdgeCount == MazeDirections.Count;
		}
	}

	public MovementDirections RandomUninitializedDirection 
    {
		get {
			int skips = Random.Range(0, MazeDirections.Count - initializedEdgeCount);
			
            for (int i = 0; i < MazeDirections.Count; i++) {
				if (edges[i] == null)
                {
					if (skips == 0) 
                    {
						return (MovementDirections)i;
					}
					skips -= 1;
				}
			}
            // throw - a way to escape loops?
			throw new System.InvalidOperationException("MazeCell has no uninitialized directions left.");
		}
	}

	public void Initialize (SegmentRoom room) {
		room.Add(this);		
	}

	public CellEdge GetEdge (MovementDirections direction) {
		return edges[(int)direction];
	}

    public void SetNMLink(MovementDirections _dir, bool _biDir)
    {
        switch(_dir)
        {

            case MovementDirections.Front:
            {
                link_f.enabled = true;
                link_f.bidirectional = _biDir;
                break;
            }
            case MovementDirections.Right:
            {
                link_r.enabled = true;
                link_r.bidirectional = _biDir;
                break;
            }
            case MovementDirections.Back:
            {
                link_b.enabled = true;
                link_b.bidirectional = _biDir;
                break;
            }
            case MovementDirections.Left:
            {
                link_l.enabled = true;
                link_l.bidirectional = _biDir;
                break;
            }
        }
    }

    public CellEdge[] GetEdges() { return edges; }
	public void SetEdge (MovementDirections direction, CellEdge edge) {
		if(edges[(int)direction] != null) {
            Destroy(edges[(int)direction].gameObject);
            initializedEdgeCount -= 1;
        }
        edges[(int)direction] = edge;
		initializedEdgeCount += 1;
	}    

	public void Show () {
		gameObject.SetActive(true);
	}

	public void Hide () {
		gameObject.SetActive(false);
	}

    public void CheckCorners()
    {
        if (GetEdge(MovementDirections.Front) is CellPassage && GetEdge(MovementDirections.Right) is CellPassage)
        {                    
            MovementDirections checkDirectiom = MovementDirections.Front;
            SegmentCell neigbhor1 = generator.GetCell(new Vector2Int(coordinates.x, coordinates.y + 1));
            SegmentCell neighbor2 = generator.GetCell(new Vector2Int(coordinates.x + 1, coordinates.y));            
            AddCorner(checkDirectiom, neigbhor1, neighbor2); ;
        }
        if (GetEdge(MovementDirections.Right) is CellPassage && GetEdge(MovementDirections.Back) is CellPassage)
        {
            MovementDirections checkDirectiom = MovementDirections.Right;
            SegmentCell neigbhor1 = generator.GetCell(new Vector2Int(coordinates.x + 1, coordinates.y));
            SegmentCell neighbor2 = generator.GetCell(new Vector2Int(coordinates.x, coordinates.y -1 ));
            AddCorner(checkDirectiom, neigbhor1, neighbor2); ;
        }
        if (GetEdge(MovementDirections.Back) is CellPassage && GetEdge(MovementDirections.Left) is CellPassage)
        {
            MovementDirections checkDirectiom = MovementDirections.Back;
            SegmentCell neigbhor1 = generator.GetCell(new Vector2Int(coordinates.x, coordinates.y -1 ));
            SegmentCell neighbor2 = generator.GetCell(new Vector2Int(coordinates.x - 1, coordinates.y));
            AddCorner(checkDirectiom, neigbhor1, neighbor2);            
        }
        if (GetEdge(MovementDirections.Left) is CellPassage && GetEdge(MovementDirections.Front) is CellPassage)
        {
            MovementDirections checkDirectiom = MovementDirections.Left;
            SegmentCell neigbhor1 = generator.GetCell(new Vector2Int(coordinates.x - 1, coordinates.y));
            SegmentCell neighbor2 = generator.GetCell(new Vector2Int(coordinates.x, coordinates.y + 1));
            AddCorner(checkDirectiom, neigbhor1, neighbor2);
        }        
    }

    private bool AddCorner(MovementDirections checkDirectiom, SegmentCell neigbhor1, SegmentCell neigbhor2)
    {
        if (!(neigbhor1.GetEdge(checkDirectiom.GetNextClockwise()) is CellPassage) && !(neigbhor2.GetEdge(checkDirectiom) is CellPassage))
            {
            Transform newCorner = Instantiate(generator.cornerPrefab).transform;

            newCorner.parent = transform;
            newCorner.localPosition = Vector3.zero;
            newCorner.localRotation = checkDirectiom.ToRotation();
            newCorner.localScale = Vector3.one;
            return true;
        }
        return false;
    }       
}